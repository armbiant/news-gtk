window.addEventListener('load', recalculate_img_hover);
window.addEventListener('resize', recalculate_img_hover);

function recalculate_img_hover() {

    var images = document.getElementsByTagName('img');
    for (var i = 0; i < images.length; i++)
    {
        var image = images[i];

        if (image.parentElement.tagName.toLocaleLowerCase() == 'a')
        {
            var href_lowercase = image.parentElement.href.toLocaleLowerCase();
            if (href_lowercase.endsWith(".jpg") || href_lowercase.endsWith(".jpeg") || href_lowercase.endsWith(".png"))
            {
                image.setAttribute(PARENT_URL_ATTRIBUTE, image.parentElement.href);
                image.parentElement.removeAttribute("href");
                add_listeners(image);
            }
        } else if (image.hasAttribute("srcset"))
        {
            add_listeners(image);
        }
        else if (image.naturalWidth > 250 || image.naturalHeight > 250)
        {
            var wRatio = image.width / image.naturalWidth;
            var hRatio = image.height / image.naturalHeight;

            if(hRatio <= SCALING_THRESHOLD || wRatio <= SCALING_THRESHOLD)
                add_listeners(image);
            else
                remove_listeners(image);
        }
    }
};


function add_listeners(element) {
    if (!element.hasAttribute(LISTENING_ATTRIBUTE))
    {
        element.setAttribute(LISTENING_ATTRIBUTE, "true");
        element.addEventListener("mouseover", on_enter);
        element.addEventListener("mouseout", on_leave);
        element.addEventListener("click", on_click);
    }
}

function remove_listeners(element) {
    if (element.hasAttribute(LISTENING_ATTRIBUTE))
    {
        element.removeAttribute(LISTENING_ATTRIBUTE)
        element.removeEventListener("mouseover", on_enter);
        element.removeEventListener("mouseout", on_leave);
        element.removeEventListener("click", on_click);
    }
}

function on_enter(event) {
    var image = event.target;
    image.classList.add("clickable-img-hover");
}

function on_leave(event) {
    var image = event.target;
    image.classList.remove("clickable-img-hover");
}

function on_click(event) {
    var image = event.target;

    if (image.hasAttribute(PARENT_URL_ATTRIBUTE))
    {
        window.webkit.messageHandlers.imageDialog.postMessage(image.getAttribute(PARENT_URL_ATTRIBUTE));
    }
    else if (image.hasAttribute("srcset"))
    {
        var srcsetList = parse_srcset(image.srcset);

        var valueList;
        if (srcsetList[0].width !== undefined)
            valueList = srcsetList.map(o => o.width);
        else
            valueList = srcsetList.map(o => o.density);

        var maxValue = Math.max(...valueList)
        var index = valueList.indexOf(maxValue);

        window.webkit.messageHandlers.imageDialog.postMessage(srcsetList[index].url);
    }
    else
    {
        window.webkit.messageHandlers.imageDialog.postMessage(image.src);
    }
}

const LISTENING_ATTRIBUTE = "listening";
const PARENT_URL_ATTRIBUTE = "parenturl";
const SCALING_THRESHOLD = 0.8;
const SRCSEG = /(\S*[^,\s])(\s+([\d.]+)(x|w))?,?/g;
const DescriptorNames = { w: "width", x: "density" };

const parse_srcset = (srcset) => matchAll(srcset, SRCSEG).map(([, url, , value, modifier]) => {
    let modKey = DescriptorNames[modifier];
    return modKey ? { url, [modKey]: parseFloat(value) } : { url };
});

const matchAll = (str, regex) => {
    let match = null, result = [];
    while ((match = regex.exec(str)) !== null)
        result.push(match);
    return result;
};