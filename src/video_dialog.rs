use crate::{app::App, util::constants::YOUTUBE_NEEDLE};
use gdk4::{Cursor, Paintable, Rectangle, Texture};
use gio::{ListStore, SimpleAction, SimpleActionGroup};
use glib::{clone, subclass, ControlFlow, Propagation, SignalHandlerId, SourceId};
use gstgtk4::PaintableSink;
use gstreamer::{
    bus::BusWatchGuard,
    prelude::{ElementExtManual, GstBinExtManual},
    traits::{ElementExt, GstBinExt, GstObjectExt, PadExt},
    ClockTime, Element, ElementFactory, MessageView, Pipeline, SeekFlags, State,
};
use gstreamer_pbutils::Discoverer;
use gtk4::{
    prelude::*, subclass::prelude::*, Adjustment, Box, Button, CompositeTemplate, ContentFit, DropDown,
    EventControllerMotion, EventControllerScroll, GestureClick, Label, NamedAction, Overlay, Picture, Popover,
    PositionType, Scale, ScaleButton, Shortcut, Stack, Widget, Window,
};
use libadwaita::{subclass::prelude::AdwWindowImpl, Window as AdwWindow};
use std::{
    cell::{Cell, RefCell},
    time::Duration,
};
use url::Url;

use rusty_ytdl::{Chapter, Video, VideoInfo};

use self::stream_gobject::VideoStream;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/media/video_dialog.blp")]
    pub struct VideoDialog {
        #[template_child]
        pub video_stack: TemplateChild<Stack>,
        #[template_child]
        pub picture: TemplateChild<Picture>,
        #[template_child]
        pub preview: TemplateChild<Picture>,
        #[template_child]
        pub main_overlay: TemplateChild<Overlay>,
        #[template_child]
        pub controls: TemplateChild<Box>,
        #[template_child]
        pub play_button: TemplateChild<Button>,
        #[template_child]
        pub timeline: TemplateChild<Scale>,
        #[template_child]
        pub timeline_popover: TemplateChild<Popover>,
        #[template_child]
        pub popover_time_label: TemplateChild<Label>,
        #[template_child]
        pub popover_chapter_label: TemplateChild<Label>,
        #[template_child]
        pub time_label: TemplateChild<Label>,
        #[template_child]
        pub volume_button: TemplateChild<ScaleButton>,
        #[template_child]
        pub volume_scroll: TemplateChild<EventControllerScroll>,
        #[template_child]
        pub volume_right_click: TemplateChild<GestureClick>,
        #[template_child]
        pub adjustment: TemplateChild<Adjustment>,
        #[template_child]
        pub stream_dropdown: TemplateChild<DropDown>,
        #[template_child]
        pub stream_list_store: TemplateChild<ListStore>,
        #[template_child]
        pub buffer_label: TemplateChild<Label>,
        #[template_child]
        pub skip_label: TemplateChild<Label>,
        #[template_child]
        pub close_shortcut: TemplateChild<Shortcut>,
        #[template_child]
        pub play_shortcut: TemplateChild<Shortcut>,
        #[template_child]
        pub skip_plus_shortcut: TemplateChild<Shortcut>,
        #[template_child]
        pub skip_minus_shortcut: TemplateChild<Shortcut>,
        #[template_child]
        pub motion_event: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub timeline_motion_event: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub control_motion: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub fullscreen_click: TemplateChild<GestureClick>,

        pub pipeline: RefCell<Option<Pipeline>>,
        pub bus_watch: RefCell<Option<BusWatchGuard>>,
        pub timeline_signal: RefCell<Option<SignalHandlerId>>,
        pub hide_timeout: RefCell<Option<SourceId>>,
        pub inihibit_control_hide: Cell<bool>,
        pub volume_before_mute: Cell<f64>,
        pub pointer_position: RefCell<Option<(f64, f64)>>,
        pub video_stream_uri: RefCell<Option<String>>,
        pub audio_stream_uri: RefCell<Option<String>>,
        pub qos_messages: Cell<u32>,
        pub qos_error_reset: RefCell<Option<SourceId>>,
        pub skip_label_reset: RefCell<Option<SourceId>>,
        pub chapters: RefCell<Vec<Chapter>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for VideoDialog {
        const NAME: &'static str = "VideoDialog";
        type ParentType = AdwWindow;
        type Type = super::VideoDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for VideoDialog {}

    impl WidgetImpl for VideoDialog {}

    impl WindowImpl for VideoDialog {}

    impl AdwWindowImpl for VideoDialog {}
}

glib::wrapper! {
    pub struct VideoDialog(ObjectSubclass<imp::VideoDialog>)
        @extends Widget, gtk4::Window, AdwWindow;
}

impl VideoDialog {
    pub fn new_url(url: &Url) -> Self {
        VideoStream::ensure_type();

        let dialog = glib::Object::new::<Self>();
        dialog.set_transient_for(Some(&App::default().main_window()));
        dialog.setup_actions();
        dialog.setup_fullscreen();
        dialog.discover_url_info(url);
        let imp = dialog.imp();

        let signal_id = imp.timeline.connect_value_changed(clone!(@strong dialog => @default-panic, move |scale| {
            let imp = dialog.imp();

            if let Some(pipeline) = imp.pipeline.borrow().as_ref() {
                if let Err(error) = pipeline.seek_simple(SeekFlags::FLUSH, scale.value() as u64 * ClockTime::MSECOND) {
                    log::error!("{error}");
                }
            }
        }));
        imp.timeline_signal.replace(Some(signal_id));

        glib::timeout_add_local(
            Duration::from_millis(100),
            clone!(@weak-allow-none dialog => @default-panic, move || {
                if let Some(dialog) = dialog.as_ref() {
                    let imp = dialog.imp();
                    if let Some(pipeline) = imp.pipeline.borrow().as_ref() {
                        if pipeline.current_state() == State::Playing {
                            if let Some(position) = pipeline.query_position::<ClockTime>() {
                                if let Some(duration) = pipeline.query_duration::<ClockTime>() {
                                    dialog.set_timeline(position.mseconds() as f64);
                                    imp.time_label.set_text(&Self::format_position_duration(position.into(), duration.into()));
                                }
                            }
                        }
                    }
                    ControlFlow::Continue
                } else {
                    ControlFlow::Break
                }
            }),
        );

        imp.motion_event.connect_motion(clone!(@weak dialog => @default-panic, move |_controller, x, y| {
            let imp = dialog.imp();

            if imp.video_stack.visible_child_name().map(|name| name.as_str() != "video").unwrap_or(false) {
                return;
            }

            if let Some((prev_x, prev_y)) = imp.pointer_position.borrow().as_ref() {
                if *prev_x == x && *prev_y == y {
                    return;
                }
            }

            imp.pointer_position.replace(Some((x, y)));

            if let Some(timeout_id) = imp.hide_timeout.take() {
                timeout_id.remove();
            }

            let timeout_id = glib::timeout_add_local(Duration::from_millis(2000), clone!(@weak dialog => @default-panic, move || {
                let imp = dialog.imp();
                imp.hide_timeout.take();

                if imp.inihibit_control_hide.get() {
                    return ControlFlow::Break
                }

                imp.controls.set_visible(false);
                dialog.set_cursor(Cursor::from_name("none", None).as_ref());
                ControlFlow::Break
            }));
            imp.hide_timeout.replace(Some(timeout_id));
            dialog.imp().controls.set_visible(true);
            dialog.set_cursor(None);
        }));

        imp.timeline_motion_event
            .connect_enter(clone!(@weak dialog => @default-panic, move |_motion, _x, _y| {
                dialog.imp().timeline_popover.popup();
            }));
        imp.timeline_motion_event
            .connect_leave(clone!(@weak dialog => @default-panic, move |_motion| {
                dialog.imp().timeline_popover.popdown();
            }));
        imp.timeline_motion_event
            .connect_motion(clone!(@weak dialog => @default-panic, move |_motion, x, _y| {
                let imp = dialog.imp();
                imp.timeline_popover.set_pointing_to(Some(&Rectangle::new(x as i32 + 13, -13, 0, 0)));
                let range_rect = imp.timeline.range_rect();
                let lower = imp.adjustment.lower();
                let upper = imp.adjustment.upper();
                let page_size = imp.adjustment.page_size();
                let progress = (x - range_rect.x() as f64) / range_rect.width() as f64;
                let progress = progress.clamp(0.0, 1.0);
                let time = lower + progress * (upper - lower - page_size);
                let duration = Duration::from_millis(time as u64);

                imp.popover_chapter_label.set_visible(!imp.chapters.borrow().is_empty());
                if let Some(current_chapter) = imp.chapters.borrow().iter().rev().find(|chapter| duration > Duration::from_secs(chapter.start_time as u64)) {
                    imp.popover_chapter_label.set_text(&current_chapter.title);
                }

                let text = Self::format_duration(duration, false, true);
                imp.popover_time_label.set_text(&text);
            }));
        imp.control_motion
            .connect_enter(clone!(@weak dialog => @default-panic, move |_motion, _x, _y| {
                dialog.imp().inihibit_control_hide.set(true);
            }));
        imp.control_motion
            .connect_leave(clone!(@weak dialog => @default-panic, move |_motion| {
                dialog.imp().inihibit_control_hide.set(false);
            }));

        imp.fullscreen_click
            .connect_released(clone!(@weak dialog => @default-panic, move |_event, times, _x, _y| {
                if times != 2 {
                    return;
                }

                if dialog.is_fullscreen() {
                    dialog.unfullscreen();
                } else {
                    dialog.set_modal(false);
                }
            }));

        imp.volume_button.set_icons(&[
            "audio-volume-muted-symbolic",
            "audio-volume-high-symbolic",
            "audio-volume-low-symbolic",
            "audio-volume-medium-symbolic",
        ]);
        imp.volume_button
            .plus_button()
            .connect_clicked(clone!(@weak dialog => @default-panic, move |_button| {
                let adj = dialog.imp().volume_button.adjustment();
                adj.set_value(f64::min(1.0, adj.value() + 0.1));
            }));
        imp.volume_button
            .minus_button()
            .connect_clicked(clone!(@weak dialog => @default-panic, move |_button| {
                let adj = dialog.imp().volume_button.adjustment();
                adj.set_value(f64::max(0.0, adj.value() - 0.1));
            }));
        imp.volume_scroll
            .connect_scroll(clone!(@weak dialog => @default-panic, move |_event, _dx, dy| {
                let adj = dialog.imp().volume_button.adjustment();
                adj.set_value(f64::min(1.0, f64::max(0.0, adj.value() - 0.1 * dy)));
                Propagation::Proceed
            }));
        imp.volume_right_click
            .connect_pressed(clone!(@weak dialog => @default-panic, move |_event, n_press, _x, _y| {
                if n_press != 1 {
                    return;
                }

                let imp = dialog.imp();
                let adj = imp.volume_button.adjustment();
                if adj.value() != 0.0 {
                    imp.volume_before_mute.set(adj.value());
                    adj.set_value(0.0);
                } else {
                    adj.set_value(imp.volume_before_mute.get());
                }
            }));
        let popup = imp.volume_button.popup().downcast::<Popover>().unwrap();
        popup.set_position(PositionType::Top);
        popup.connect_show(clone!(@weak dialog => @default-panic, move |_popup| {
            dialog.imp().inihibit_control_hide.set(true);
        }));
        popup.connect_hide(clone!(@weak dialog => @default-panic, move |_popup| {
            dialog.imp().inihibit_control_hide.set(false);
        }));
        imp.volume_button
            .connect_value_changed(clone!(@strong dialog => @default-panic, move |_button, value| {
                let imp = dialog.imp();
                if let Some(pipeline) = imp.pipeline.borrow().as_ref() {
                    if let Some(volume) = pipeline.by_name("volume") {
                        volume.set_property("volume", value);
                    }
                }
            }));

        dialog.connect_close_request(|dialog| {
            if let Some(pipe) = dialog.imp().pipeline.take() {
                _ = pipe.set_state(State::Null);
            }
            Propagation::Proceed
        });

        dialog
    }

    fn has_pipeline(&self) -> bool {
        self.imp().pipeline.borrow().is_some()
    }

    fn create_pipeline(&self) {
        let imp = self.imp();

        let pipeline = Pipeline::new();
        let video_src = ElementFactory::make("uridecodebin")
            .name("video-src")
            //.property("download", true)
            .property("uri", imp.video_stream_uri.borrow().as_deref().unwrap_or(""))
            .build()
            .expect("failed to create uridecodebin");
        let audio_src = imp.audio_stream_uri.borrow().as_deref().map(|audio_stream_uri| {
            ElementFactory::make("uridecodebin")
                .name("audio-src")
                //.property("download", true)
                .property("uri", audio_stream_uri)
                .build()
                .expect("failed to create uridecodebin")
        });
        let video_convert = ElementFactory::make("videoconvert")
            .build()
            .expect("failed to create video converter");
        let gtk_sink = ElementFactory::make("gtk4paintablesink")
            .build()
            .expect("failed to create gtk paintable sink");
        let audio_convert = ElementFactory::make("audioconvert")
            .build()
            .expect("failed to create audio converter");
        let audio_resample = ElementFactory::make("audioresample")
            .build()
            .expect("failed to create audio resampler");
        let volume = ElementFactory::make("volume")
            .name("volume")
            .property("volume", imp.volume_button.value())
            .build()
            .expect("failed to create audio resampler");
        let audio_sink = ElementFactory::make("autoaudiosink")
            .build()
            .expect("failed to create audio sink");

        pipeline
            .add_many([
                &video_src,
                &video_convert,
                &gtk_sink,
                &audio_convert,
                &audio_resample,
                &volume,
                &audio_sink,
            ])
            .expect("failed to add elements to pipeline");
        if let Some(audio_src) = &audio_src {
            pipeline.add(audio_src).expect("failed to add audio source to pipeline");

            let audio_convert = audio_convert.clone();
            audio_src.connect_pad_added(move |_src, pad| {
                if pad.is_linked() {
                    return;
                }

                if let Some(caps) = pad.current_caps() {
                    let caps_string = caps.to_string();
                    if caps_string.starts_with("audio/x-raw") {
                        if let Some(sink_pad) = audio_convert.static_pad("sink") {
                            if !sink_pad.is_linked() {
                                if let Err(error) = pad.link(&sink_pad) {
                                    log::error!("Failed to link audio pipeline: {error}")
                                }
                            }
                        }
                    }
                }
            });
        }
        video_convert
            .link(&gtk_sink)
            .expect("failed to link video convterer to gtk paintable sink");
        audio_convert
            .link(&audio_resample)
            .expect("failed to link audio converter to gtk audio resampler");
        audio_resample
            .link(&volume)
            .expect("failed to audio resampler to volume");
        volume.link(&audio_sink).expect("failed to link volume to audio sink");

        video_src.connect_pad_added(move |_src, pad| {
            if pad.is_linked() {
                return;
            }

            if let Some(caps) = pad.current_caps() {
                let caps_string = caps.to_string();
                if caps_string.starts_with("video/x-raw") {
                    if let Some(sink_pad) = video_convert.static_pad("sink") {
                        if !sink_pad.is_linked() {
                            if let Err(error) = pad.link(&sink_pad) {
                                log::error!("Failed to link video pipeline: {error}")
                            }
                        }
                    }
                } else if caps_string.starts_with("audio/x-raw") && audio_src.is_none() {
                    if let Some(sink_pad) = audio_convert.static_pad("sink") {
                        if !sink_pad.is_linked() {
                            if let Err(error) = pad.link(&sink_pad) {
                                log::error!("Failed to link audio pipeline: {error}")
                            }
                        }
                    }
                }
            }
        });

        let gtk_sink = gtk_sink
            .dynamic_cast::<PaintableSink>()
            .expect("gtk_sink is not a PaintableSink");
        let paintable = gtk_sink.property::<Paintable>("paintable");
        imp.picture.set_paintable(Some(&paintable));
        imp.volume_button.set_value(volume.property("volume"));

        let bus = pipeline.bus().expect("failed to get pipeline message bus");
        let bus_watch = bus
            .add_watch_local(clone!(@strong self as dialog => @default-panic, move |_, msg| {
                match msg.view() {
                    MessageView::Error(err) => {
                        log::error!(
                            "Error from {}: {} ({:?})",
                            Self::obj_name(err.src()),
                            err.error(),
                            err.debug()
                        );
                    }
                    MessageView::Info(info) => {
                        log::debug!("{info:?}");
                    }
                    MessageView::Eos(_) => log::debug!("stream ended"),
                    MessageView::Warning(msg) => log::warn!("{:?}", msg.details()),
                    MessageView::Tag(msg) => log::debug!("tag {} {:?}", Self::obj_name(msg.src()), msg.tags()),
                    MessageView::Buffering(msg) => {
                        let percent = msg.percent();
                        if percent == 100 {
                            dialog.imp().buffer_label.set_visible(false);
                        } else {
                            dialog.imp().buffer_label.set_visible(true);
                            dialog.imp().buffer_label.set_text(&format!("{}%", msg.percent()));
                        }
                    },
                    MessageView::StateChanged(msg) => {
                        log::debug!(
                            "state change {}: old: {:?} - current: {:?} - pending {:?}",
                            Self::obj_name(msg.src()),
                            msg.old(),
                            msg.current(),
                            msg.pending()
                        );

                        if msg.current() == State::Playing || msg.current() == State::Paused || msg.current() == State::Null {
                            dialog.imp().buffer_label.set_visible(false);
                        }
                    }
                    MessageView::StateDirty(_) => todo!(),
                    MessageView::StepDone(_) => todo!(),
                    MessageView::ClockProvide(_) => todo!(),
                    MessageView::ClockLost(_) => todo!(),
                    MessageView::NewClock(msg) => log::debug!("new clock {}", Self::obj_name(msg.src())),
                    MessageView::StructureChange(_) => todo!(),
                    MessageView::StreamStatus(msg) => {
                        let (status, element) = msg.get();
                        log::debug!("stream status '{}': {:?}", element.name(), status);
                    }
                    MessageView::Application(_) => todo!(),
                    MessageView::Element(msg) => log::debug!("new element {}", Self::obj_name(msg.src())),
                    MessageView::SegmentStart(_) => todo!(),
                    MessageView::SegmentDone(_) => todo!(),
                    MessageView::DurationChanged(msg) => {
                        if let Some(duration) = msg.src().unwrap().dynamic_cast_ref::<Element>().unwrap().query_duration::<ClockTime>() {
                            let mseconds = duration.mseconds();
                            dialog.imp().adjustment.set_upper(mseconds as f64);
                        }
                    },
                    MessageView::Latency(msg) => {
                        log::debug!("{} latency changed", Self::obj_name(msg.src()));
                    },
                    MessageView::AsyncStart(_) => todo!(),
                    MessageView::AsyncDone(msg) => log::debug!("async done {}", Self::obj_name(msg.src())),
                    MessageView::RequestState(_) => todo!(),
                    MessageView::StepStart(_) => todo!(),
                    MessageView::Qos(msg) => {
                        let (jitter, proportion, quality) = msg.values();
                        log::warn!("jitter: {jitter} - proportion: {proportion} - quality: {quality}");
                        let (processed, dropped) = msg.stats();
                        log::warn!("processed: {processed} - dropped: {dropped}");

                        let imp = dialog.imp();
                        imp.qos_messages.set(imp.qos_messages.get() + 1);

                        log::warn!("qos msg count {}", imp.qos_messages.get());

                        if let Some(source_id) = imp.qos_error_reset.take() {
                            source_id.remove();
                        }

                        if imp.qos_messages.get() > 1500 {
                            dialog.destroy_pipeline();
                            imp.play_button.set_icon_name("media-playback-start-symbolic");
                            imp.adjustment.set_value(0.0);
                            dialog.create_pipeline();
                        }

                        let source_id = glib::timeout_add_local(Duration::from_millis(500), clone!(@weak dialog => @default-panic, move || {
                            let imp = dialog.imp();
                            imp.qos_messages.set(0);
                            imp.qos_error_reset.take();
                            ControlFlow::Break
                        }));
                        imp.qos_error_reset.replace(Some(source_id));
                    },
                    MessageView::Progress(_) => todo!(),
                    MessageView::Toc(_) => todo!(),
                    MessageView::ResetTime(msg) => log::debug!("reset time: {}", Self::obj_name(msg.src())),
                    MessageView::StreamStart(msg) => log::debug!("stream start {}", Self::obj_name(msg.src())),
                    MessageView::NeedContext(msg) => log::debug!("{} needs context ", Self::obj_name(msg.src())),
                    MessageView::HaveContext(msg) => log::debug!("{} has context", Self::obj_name(msg.src())),
                    MessageView::DeviceAdded(_) => todo!(),
                    MessageView::DeviceRemoved(_) => todo!(),
                    MessageView::PropertyNotify(_) => todo!(),
                    MessageView::StreamCollection(_) => todo!(),
                    MessageView::StreamsSelected(_) => todo!(),
                    MessageView::Redirect(_) => todo!(),
                    MessageView::DeviceChanged(_) => todo!(),
                    MessageView::InstantRateRequest(_) => todo!(),
                    MessageView::Other => todo!(),
                    _ => unreachable!(),
                };
                ControlFlow::Continue
            }))
            .expect("Failed to add bus watch");

        if let Err(error) = pipeline.set_state(State::Ready) {
            log::error!("failed to set pipeline ready: {error}");
        }

        imp.pipeline.replace(Some(pipeline));
        imp.bus_watch.replace(Some(bus_watch));
    }

    fn destroy_pipeline(&self) {
        let imp = self.imp();
        if let Some(pipeline) = imp.pipeline.take() {
            _ = pipeline.set_state(State::Null);
        }
        imp.bus_watch.take();
    }

    fn obj_name(obj: Option<&gstreamer::Object>) -> String {
        obj.and_then(|obj| obj.dynamic_cast_ref::<Element>())
            .map(|e| e.name().as_str().into())
            .unwrap_or("".into())
    }

    fn discover_url_info(&self, url: &Url) {
        if url.as_str().contains(YOUTUBE_NEEDLE) {
            self.discover_youtube(url);
        } else {
            self.discover_gstreamer(url);
        }
    }

    fn discover_youtube(&self, url: &Url) {
        App::default().execute_with_callback(
            clone!(@strong url => @default-panic, move |_, _| async move {
                if let Ok(video) = Video::new(url.as_str()) {
                    video.get_info().await.ok()
                } else {
                    None
                }
            }),
            clone!(@strong self as dialog => @default-panic, move |_app, video_info: Option<VideoInfo>| {
                let video_info = match video_info {
                    Some(info) => info,
                    None => {
                        log::error!("Failed to parse youtube video");
                        dialog.close();
                        return;
                    },
                };

                let video_only_formats = video_info.formats.iter().filter(|format| format.has_video && !format.has_audio && format.width.map(|w| w <= 1920).unwrap_or(true) && format.codecs.as_deref().map(|codec| codec.contains("vp9")).unwrap_or(false)).cloned().collect::<Vec<_>>();
                let audio_only_formats = video_info.formats.iter().filter(|format| format.has_audio && !format.has_video).cloned().collect::<Vec<_>>();

                let mut audio_format = audio_only_formats.iter().find(|format| format.audio_quality.as_deref().map(|quality| quality.contains("MEDIUM")).unwrap_or(false) && format.codecs.as_deref().map(|codec| codec == "opus").unwrap_or(false)).cloned();
                if audio_format.is_none() {
                    audio_format = audio_only_formats.get(0).cloned();
                }

                let mut selected_stream_index = 0;
                let mut video_format = None;
                for (i, format) in video_only_formats.iter().enumerate() {
                    if format.quality_label.as_deref().map(|quality| quality == "720p").unwrap_or(false) {
                        selected_stream_index = i;
                        video_format = Some(format.clone());
                    }
                }
                if video_format.is_none() {
                    selected_stream_index = 0;
                    video_format = video_only_formats.get(0).cloned();
                }

                let video_format = video_format.unwrap();
                let audio_format = audio_format.unwrap();

                let imp = dialog.imp();
                for video_stream in video_only_formats {
                    let video_stream = VideoStream::new(video_stream.quality_label, video_stream.url);
                    imp.stream_list_store.append(&video_stream);
                }
                imp.stream_dropdown.set_visible(true);
                imp.stream_dropdown.set_selected(selected_stream_index as u32);
                imp.stream_dropdown.connect_selected_item_notify(clone!(@weak dialog => @default-panic, move |dropdown| {
                    if let Some(selected_item) = dropdown.selected_item().and_downcast::<VideoStream>() {
                        if !dialog.has_pipeline() {
                            dialog.create_pipeline();
                        }

                        let imp = dialog.imp();
                        log::info!("selected {}", selected_item.name());
                        if let Some(pipeline) = imp.pipeline.borrow().as_ref() {
                            if let Some(video_src) = pipeline.by_name("video-src") {
                                video_src.set_property("uri", selected_item.url());
                                imp.video_stream_uri.replace(Some(selected_item.url()));
                            }
                        }
                    }
                }));
                imp.video_stream_uri.replace(Some(video_format.url.clone()));
                imp.audio_stream_uri.replace(Some(audio_format.url));

                imp.chapters.replace(video_info.video_details.chapters.clone());
                for chapter in video_info.video_details.chapters {
                    if chapter.start_time == 0 {
                        continue;
                    }

                    let start = ClockTime::from_seconds(chapter.start_time as u64);
                    imp.timeline.add_mark(start.mseconds() as f64, PositionType::Bottom, None)
                }

                let width = video_format.width.unwrap_or(640);
                let height = video_format.height.unwrap_or(480);
                let mseconds = video_format.approx_duration_ms.as_deref().and_then(|s| s.parse::<u64>().ok()).unwrap_or(0);
                imp.adjustment.set_upper(mseconds as f64);
                imp.time_label.set_text(&Self::format_position_duration(Duration::from_millis(0), Duration::from_millis(mseconds)));
                let thumbnails = video_info.video_details.thumbnails;
                let mut biggest_thumbnail_size = 0;
                let mut biggest_thumbnail = None;
                for thumbnail in thumbnails {
                    let size = thumbnail.width * thumbnail.height;
                    if size > biggest_thumbnail_size {
                        biggest_thumbnail_size = size;
                        biggest_thumbnail = Some(thumbnail);
                    }
                }

                if let Some(thumbnail) = biggest_thumbnail {
                    let url = thumbnail.url;
                    App::default().execute_with_callback(
                        |_news_flash, client| async move {
                            let response = client.get(url).send().await.unwrap().bytes().await.unwrap();
                            response.to_vec()
                        },
                        clone!(@weak dialog => @default-panic, move |_app, res| {
                            let bytes  = glib::Bytes::from_owned(res);
                            let texture = Texture::from_bytes(&bytes).unwrap();

                            let imp = dialog.imp();
                            imp.preview.set_paintable(Some(&texture));
                            imp.video_stack.set_visible_child_name("preview");
                            imp.controls.set_visible(true);
                        })
                    );
                }

                let (width, height) = dialog.calculate_size(width as i32, height as i32);
                dialog.set_width_request(width);
                dialog.set_height_request(height);
            }),
        );
    }

    fn discover_gstreamer(&self, url: &Url) {
        App::default().execute_with_callback(
            clone!(@strong url => @default-panic, move |_news_flash, _client| async move {
                let discoverer = Discoverer::new(ClockTime::from_seconds(5)).unwrap();
                let info = discoverer.discover_uri(url.as_str()).unwrap();
                let streams = info.video_streams();
                let stream = streams.get(0).unwrap();
                let width = stream.width();
                let height = stream.height();
                (width, height)
            }),
            clone!(@weak self as dialog, @strong url => @default-panic, move |_app, (width, height)| {
                let (width, height) = dialog.calculate_size(width as i32, height as i32);
                dialog.set_width_request(width);
                dialog.set_height_request(height);

                let imp = dialog.imp();
                imp.video_stream_uri.replace(Some(url.as_str().into()));
                imp.video_stack.set_visible_child_name("video");
                imp.controls.set_visible(true);
            }),
        );
    }

    fn setup_actions(&self) {
        let imp = self.imp();
        imp.close_shortcut.set_action(Some(NamedAction::new("window.escape")));
        imp.play_shortcut
            .set_action(Some(NamedAction::new("window.toggle-play")));
        imp.skip_plus_shortcut
            .set_action(Some(NamedAction::new("window.skip-plus")));
        imp.skip_minus_shortcut
            .set_action(Some(NamedAction::new("window.skip-minus")));

        let escape_action = SimpleAction::new("escape", None);
        escape_action.connect_activate(
            clone!(@weak self as dialog => @default-panic, move |_action, _parameter| {
                if dialog.is_fullscreen() {
                    dialog.unfullscreen();
                } else {
                    dialog.close();
                }
            }),
        );

        let fullscreen_action = SimpleAction::new("fullscreen", None);
        fullscreen_action.connect_activate(
            clone!(@weak self as dialog => @default-panic, move |_action, _parameter| {
                if dialog.is_fullscreen() {
                    dialog.unfullscreen();
                } else {
                    dialog.set_modal(false);
                }
            }),
        );

        let skip_plus_action = SimpleAction::new("skip-plus", None);
        skip_plus_action.connect_activate(
            clone!(@strong self as dialog => @default-panic, move |_action, _parameter| {
                dialog.skip_seconds(5);
            }),
        );

        let skip_minus_action = SimpleAction::new("skip-minus", None);
        skip_minus_action.connect_activate(
            clone!(@strong self as dialog => @default-panic, move |_action, _parameter| {
                dialog.skip_seconds(-5);
            }),
        );

        let toggle_play_action = SimpleAction::new("toggle-play", None);
        toggle_play_action.connect_activate(
            clone!(@strong self as dialog => @default-panic, move |_action, _parameter| {
                if !dialog.has_pipeline() {
                    dialog.create_pipeline();
                }

                let imp = dialog.imp();
                if let Some(pipe) = imp.pipeline.borrow().as_ref() {
                    let (_res, state, _pending) = pipe.state(None);
                    if let Some(name) = imp.video_stack.visible_child_name() {
                        if name.as_str() != "video" {
                            imp.video_stack.set_visible_child_name("video");
                        }
                    }

                    if state == State::Playing {
                        if let Err(error) = pipe.set_state(State::Paused) {
                            log::error!("failed to pause pipeline: {error}");
                        }
                        imp.play_button.set_icon_name("media-playback-start-symbolic");
                    } else {
                        if let Err(error) = pipe.set_state(State::Playing) {
                            log::error!("failed to start pipeline: {error}");
                        }
                        imp.timeline.set_sensitive(true);
                        imp.stream_dropdown.set_sensitive(false);
                        imp.play_button.set_icon_name("media-playback-pause-symbolic");
                    }
                }
            }),
        );

        let stop_action = SimpleAction::new("stop", None);
        stop_action.connect_activate(
            clone!(@strong self as dialog => @default-panic, move |_action, _parameter| {

                dialog.destroy_pipeline();

                let imp = dialog.imp();
                imp.play_button.set_icon_name("media-playback-start-symbolic");
                imp.adjustment.set_value(0.0);
                imp.timeline.set_sensitive(false);
                imp.stream_dropdown.set_sensitive(true);

                dialog.create_pipeline();
            }),
        );

        let action_group = SimpleActionGroup::new();
        action_group.add_action(&escape_action);
        action_group.add_action(&fullscreen_action);
        action_group.add_action(&toggle_play_action);
        action_group.add_action(&stop_action);
        action_group.add_action(&skip_plus_action);
        action_group.add_action(&skip_minus_action);
        self.insert_action_group("window", Some(&action_group));
    }

    fn skip_seconds(&self, seconds: i32) {
        let imp = self.imp();

        if let Some(source_id) = imp.skip_label_reset.take() {
            source_id.remove();
        }

        let source_id = glib::timeout_add_local(
            Duration::from_millis(500),
            clone!(@weak self as dialog => @default-panic, move || {
                let imp = dialog.imp();
                imp.skip_label_reset.take();
                imp.skip_label.set_visible(false);
                ControlFlow::Break
            }),
        );
        imp.skip_label_reset.replace(Some(source_id));

        if let Some(pipeline) = imp.pipeline.borrow().as_ref() {
            if let Some(position) = pipeline.query_position::<ClockTime>() {
                let new_position = position.seconds() as i32 + seconds;
                if let Err(error) = pipeline.seek_simple(SeekFlags::FLUSH, new_position as u64 * ClockTime::SECOND) {
                    log::error!("{error}");
                }
                imp.skip_label.set_visible(true);
                let (sign, number) = if seconds > 0 { ('+', seconds) } else { ('-', -seconds) };
                imp.skip_label.set_text(&format!("{sign} {number} s"));
            }
        }
    }

    fn setup_fullscreen(&self) {
        self.connect_modal_notify(|dialog| {
            if !dialog.is_modal() {
                dialog.set_transient_for(None::<&Window>);
                dialog.fullscreen();
                dialog.imp().picture.set_content_fit(ContentFit::Contain);
            }
        });
        self.connect_fullscreened_notify(|dialog| {
            if !dialog.is_fullscreen() {
                dialog.set_transient_for(Some(&App::default().main_window()));
                dialog.set_modal(true);
                dialog.imp().picture.set_content_fit(ContentFit::Cover);
            }
        });
    }

    fn calculate_size(&self, video_width: i32, video_height: i32) -> (i32, i32) {
        let main_window = App::default().main_window();
        let parent_width = main_window.width();
        let parent_height = main_window.height();

        if video_width < parent_width && video_height < parent_height {
            return (video_width, video_height);
        }

        let mut scale = parent_width as f64 / video_width as f64;
        if video_height as f64 * scale > parent_height as f64 {
            scale = parent_height as f64 / video_height as f64;
        }
        let scale = scale * 0.95;

        let width = video_width as f64 * scale;
        let height = video_height as f64 * scale;

        (width as i32, height as i32)
    }

    fn set_timeline(&self, value: f64) {
        let imp = self.imp();

        if let Some(signal_id) = imp.timeline_signal.borrow().as_ref() {
            imp.timeline.block_signal(signal_id);
            imp.adjustment.set_value(value);
            imp.timeline.unblock_signal(signal_id);
        } else {
            imp.adjustment.set_value(value);
        }
    }

    fn format_position_duration(position: Duration, duration: Duration) -> String {
        let duration = Self::format_duration(duration, false, false);
        let len = duration.len();
        let position = Self::format_duration(position, len == 8, len >= 5);

        format!("{} / {}", position, duration)
    }

    fn format_duration(duration: Duration, force_h: bool, force_m: bool) -> String {
        let s = duration.as_secs();
        let h = s / 3600;

        let s = s - h * 3600;
        let m = s / 60;

        let s = s - m * 60;

        if h > 0 || force_h {
            format!("{:02}:{:02}:{:02}", h, m, s)
        } else if m > 0 || force_m {
            format!("{:02}:{:02}", m, s)
        } else {
            format!("{:02}", s)
        }
    }
}

mod stream_gobject {
    use super::*;
    use glib::{Object, Properties};

    mod imp {
        use glib::{ParamSpec, Value};

        use super::*;

        #[derive(Properties, Default)]
        #[properties(wrapper_type = super::VideoStream)]
        pub struct VideoStream {
            #[property(get, set)]
            name: RefCell<String>,

            #[property(get, set)]
            url: RefCell<String>,
        }

        #[glib::object_subclass]
        impl ObjectSubclass for VideoStream {
            const NAME: &'static str = "NewsFlashVideoStream";
            type Type = super::VideoStream;
        }

        impl ObjectImpl for VideoStream {
            fn properties() -> &'static [ParamSpec] {
                Self::derived_properties()
            }
            fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
                self.derived_set_property(id, value, pspec)
            }
            fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
                self.derived_property(id, pspec)
            }
        }
    }

    glib::wrapper! {
        pub struct VideoStream(ObjectSubclass<imp::VideoStream>);
    }

    impl Default for VideoStream {
        fn default() -> Self {
            Object::new()
        }
    }

    impl VideoStream {
        pub fn new(name: Option<String>, url: String) -> Self {
            let video_stream = Self::default();
            video_stream.set_property("name", name.unwrap_or("Unknown".into()));
            video_stream.set_property("url", url);
            video_stream
        }
    }
}
