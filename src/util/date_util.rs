use crate::app::App;
use crate::i18n::i18n;
use crate::util::ClockFormat;
use chrono::{Datelike, Duration, Local, NaiveDateTime, TimeZone};

pub struct DateUtil;

impl DateUtil {
    pub fn format_time(naive_utc: &NaiveDateTime) -> String {
        let local_datetime = Local.from_utc_datetime(naive_utc);
        let clock_format = App::default().desktop_settings().clock_format();

        if clock_format == ClockFormat::F12H {
            format!("{}", local_datetime.format("%I:%M %p"))
        } else {
            format!("{}", local_datetime.format("%k:%M"))
        }
    }

    pub fn format_date(naive_utc: &NaiveDateTime) -> String {
        let local_date = Local.from_utc_datetime(naive_utc).date_naive();
        let now = Local::now().naive_local().date();
        let days_since = now - local_date;
        let same_year = local_date.year() == now.year();
        let humand_readable = local_date.format("%A %B, %e"); // e.g. "Tuesday, March 5"

        if now == local_date {
            i18n("Today")
        } else if days_since == Duration::days(1) {
            i18n("Yesterday")
        } else if same_year {
            format!("{humand_readable}")
        } else {
            format!("{humand_readable} {}", local_date.format("%Y"))
        }
    }
}
